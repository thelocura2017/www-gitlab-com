---
layout: handbook-page-toc
title: "Data Catalog"
description: "The Data Catalog page indexes Analytics Dashboards, Workflows, and Terms."
---
{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

---

## GitLab Data Catalog Index

The Data Catalog page indexes Analytics Solutions, Dashboards, Workflows, and Key Terms. Please feel free to contribute to add additional links and resources.

### Marketing

* [TD: Marketing Data Mart](/handbook/business-technology/data-team/data-catalog/email-data-mart)
* [TD: SDR Performance Dashboard](https://app.periscopedata.com/app/gitlab/894285/TD-SDR-Performance-Dashboard---V1.0)

### Sales

* [TD: Customer Segmentation](/handbook/business-technology/data-team/data-catalog/customer-segmentation)
* [TD: Sales Funnel](/handbook/business-technology/data-team/data-catalog/sales-funnel)
* [Usage Data Upload](/handbook/business-technology/data-team/data-catalog/manual-data-upload/)

### Finance

* [TD: Finance ARR](/handbook/business-technology/data-team/data-catalog/finance-arr)

### Product

* [TD: Product Geolocation](/handbook/business-technology/data-team/data-catalog/product-geolocation)
* [TD: Pricing Analysis](/handbook/business-technology/data-team/data-catalog/pricing)
* [XMAU Analysis](/handbook/business-technology/data-team/data-catalog/xmau-analysis)
* [Product Usage Data](/handbook/business-technology/data-team/data-catalog/product-usage-data)

### Customer Success

* `Under Construction`

### Engineering

* [MR Rate](/handbook/engineering/performance-indicators/#engineering-mr-rate)

### People

* [People Metrics Overview](/handbook/business-technology/data-team/data-catalog/people-analytics/)
* [PTO By Roots (Slack)](/handbook/business-technology/data-team/data-catalog/people-analytics/pto/pto.html)
* [People Key Metrics](/handbook/business-technology/data-team/data-catalog/people_key_metrics_dashboard)
* [People KPI Deck](/handbook/business-technology/data-team/data-catalog/people-analytics/people_kpi_deck.htm)
* [Promotions Report](/handbook/business-technology/data-team/data-catalog/people-analytics/promotions_report.html)
* [Talent Acquisition Metrics](/handbook/business-ops/data-team/data-catalog/people-analytics/talent-acquisition-metrics.html.md)
* [People Metrics - Data Discovery in Sisense Dashboard](https://app.periscopedata.com/app/gitlab/831245/People-Data-Discovery-Feature)

### Data Team

* [Sisense Usage and Adoption](https://app.periscopedata.com/app/gitlab/topic/Sisense-Maintenance/abde7717743143098ac071be8c646bdb)
* [Trusted Data Health](https://app.periscopedata.com/app/gitlab/756199/Trusted-Data-Dashboard)

## Metrics and Terms Index

* [ARR - Annual Recurring Revenue](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/)
* [ATR - Available To Renew](https://about.gitlab.com/handbook/sales/sales-term-glossary/#available-to-renew-atr) 
* [NDR - Net Dollar Retention](https://about.gitlab.com/handbook/customer-success/vision/#retention-gross--net-dollar-weighted)
* [xMAU - x Monthly Active Users](/handbook/product/performance-indicators/#structure/)
* [Product Stage](/handbook/product/product-categories/#devops-stages)

### Legend

📊 indicates that the solution is operational and is embedded in the handbook.

🚧 indicates that the solution is in a `Work In Progress` and is actively being developed. When using this indicator, an issue should also be linked from this page.

🐔 indicates that the solution is unlikely to be operationalized in the near term.
