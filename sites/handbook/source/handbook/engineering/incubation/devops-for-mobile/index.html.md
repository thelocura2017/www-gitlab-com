---
layout: handbook-page-toc
title: DevOps for Mobile Apps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mission

Our goal is to improve the experience for Developers targeting mobile platforms by providing CI/CD capabilities and workflows that improve the experience of provisioning and deploying mobile apps written with native iOS and Android technologies. Our current focus is on [our direction](/direction/mobile/mobile-devops/) for DevOps for Mobile Applications.

## Who We Are

The DevOps for Mobile Apps SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

## How We Work

As we explore the opportunities and challenges in this space, we will share weekly demos. These demos will be recorded and shared below:

| Date                | Demo |
|---------------------|------|
| August 13, 2021     |      |
| August 20, 2021     |      |
| August 27, 2021     |      |
| September 3, 2021   |      |

## How To Contribute

#### GitLab Issues

Please feel free to create issues or participate in discussions in our [issue board](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/-/issues).

#### Slack

We can also be found in Slack at `#incubation-eng` (GitLab Internal)

